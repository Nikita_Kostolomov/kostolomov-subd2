/***************************************************************
 * Name:      Kostolomov_SUBDMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2019-03-23
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef KOSTOLOMOV_SUBDMAIN_H
#define KOSTOLOMOV_SUBDMAIN_H

//(*Headers(Kostolomov_SUBDFrame)
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
//*)

class Kostolomov_SUBDFrame: public wxFrame
{
    public:

        Kostolomov_SUBDFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~Kostolomov_SUBDFrame();

    private:

        //(*Handlers(Kostolomov_SUBDFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(Kostolomov_SUBDFrame)
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(Kostolomov_SUBDFrame)
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // KOSTOLOMOV_SUBDMAIN_H
