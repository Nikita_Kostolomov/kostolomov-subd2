/***************************************************************
 * Name:      Kostolomov_SUBDApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2019-03-23
 * Copyright:  ()
 * License:
 **************************************************************/

#include "Kostolomov_SUBDApp.h"

//(*AppHeaders
#include "Kostolomov_SUBDMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(Kostolomov_SUBDApp);

bool Kostolomov_SUBDApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	Kostolomov_SUBDFrame* Frame = new Kostolomov_SUBDFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
