/***************************************************************
 * Name:      Kostolomov_SUBDApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2019-03-23
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef KOSTOLOMOV_SUBDAPP_H
#define KOSTOLOMOV_SUBDAPP_H

#include <wx/app.h>

class Kostolomov_SUBDApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // KOSTOLOMOV_SUBDAPP_H
